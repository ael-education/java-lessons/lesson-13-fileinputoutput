/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package ru.pnu.javaio;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import ru.pnu.javaio.bookkeeping.SalaryCalculator;
import ru.pnu.javaio.database.Employee;
import ru.pnu.javaio.database.EmployeeDatabase;
import ru.pnu.javaio.database.strings.StringProcessor;
import ru.pnu.javaio.fileOperation.FileManager;

/**
 *
 * @author developer
 */
public class JavaFileIO {

    public static void main(String[] args) {
          
        System.out.println("Программа учета заработной платы");
        FileManager fileManager = new FileManager();
        File file = new File("/home/developer/git/gitlab/ael/java-lessons/lesson-13-fileinputoutput/JavaFileIO/src/main/resources" + File.separator + "employee.dat");

        fileManager.openFile(file);
        List<String> strings = fileManager.readStrings(file);
        System.out.println("Прочитано из файла [" + file.getName() + "]  " + strings.size() + "  строк");
        
        
        // База данных сотрудников
        List<Employee> employeeList = new ArrayList<>();
        
        System.out.println("Формирование базы данных сотрудников ...");
        for (int i = 0; i < strings.size(); i++) {           
            Employee employee = StringProcessor.parce(strings.get(i));                        
            if (employee != null)
            {
                
               int id =  i+1;
               employee.setId(id);
               employeeList.add(employee);
               System.out.println("СОТРУДНИК: "+employee.toString());
            }                           
        }
        
        System.out.println("\n");
        System.out.println("-------------------------------------");
        System.out.println("Сформирована БД сотрудников ["+employeeList.size()+"] записей из файла");
        System.out.println("-------------------------------------");
        
        // Сформировать экземпляр класса EmployeeDatabase и разместить список сотруников из файла 
        // в памяти ( во внутренней карте employeeMap)
        EmployeeDatabase database = new EmployeeDatabase();
        for (int i = 0; i < employeeList.size(); i++) {
            
            // Получение экземпляра сотрудника из списка
            Employee e = employeeList.get(i);            
            database.getEmployeeMap().put(i, e);
            
            //
            //database.getEmployeeMap().put(i,employeeList.get(i));            
        }
        
        
        
        System.out.println("\n");
        System.out.println("-------------------------------------");
        System.out.println("Сформирована БД сотрудников ["+database.getEmployeeMap().size()+"] записей в оперативной памяти");
        System.out.println("-------------------------------------");
        
        

        
        
        
        LocalDate ld = LocalDate.now();        
        System.out.println("\n\n Расчет зарплаты .... на ноябрь 2023 г. "+ld);
        Map<Integer, Employee> employee = database.getEmployeeMap();
        
        System.out.println("Создание файла для расчета ЗП");
        LocalDate today = LocalDate.now();
        
        System.out.println("Текущая дата: "+today);
        System.out.println("Месяц: "+today.getMonthValue());
        System.out.println("год: "+today.getYear());
        
        String salaryFileName = "emlpoyee-salary.dat"+"_"+today.getMonthValue()+"_"+today.getYear();
        
        fileManager.createFile("/home/developer/git/gitlab/ael/java-lessons/lesson-13-fileinputoutput/JavaFileIO/src/main/resources", salaryFileName);
        
        
        for (Map.Entry<Integer, Employee> entry : employee.entrySet()) {
            
            Integer key = entry.getKey();            // Чтение  ключа 
            Employee value = entry.getValue();       // Чтение значения   
            double monthSalary = SalaryCalculator.monthSalary(value.getPositionPart(), value.getSalary());
            
            System.out.println(value+ " Расчет з.п = "+monthSalary);
        }
        
        
        
        
        fileManager.closeFile(file);
        
        String fileName = "emloyee-salary.dat";
        String folder = "/home/developer/git/gitlab/ael/java-lessons/lesson-13-fileinputoutput/JavaFileIO/src/main/resources";
        
        System.out.println("Создание нового файла ["+fileName+"] ...");
        
        fileManager.createFile(folder, fileName);
        
        
        
    }
}
