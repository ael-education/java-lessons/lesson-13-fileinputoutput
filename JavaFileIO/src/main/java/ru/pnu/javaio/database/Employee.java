/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.pnu.javaio.database;

import java.time.LocalDate;

/**
 *
 * @author developer
 */
public class Employee {

    private int id; // 0. Идентифкатор сотруника
    
    private String sureName;        // 1. Фамилия   
    private String firstName;       // 2. Имя 
    private String middleName;      // 3. Отчество  
    private String position;        // 4. Должность
    private double positionPart;    // 5. Ставка 
    private double salary;          // 6. Оклад по должности     
    private LocalDate  period;      // 7. Расчетный период
    private double sum;             // 8. Сумма

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    
    
    
    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%-25s","Табельный номер ["+id+"]:"));
        sb.append(String.format("%-15s",sureName));
        sb.append(String.format("%-15s",firstName));
        sb.append(String.format("%-15s",middleName));
        sb.append(String.format("%-25s","["+position+"]"));
        sb.append(String.format("%-7s",""+positionPart+""));
        sb.append(String.format("%-10s",""+salary+" руб."));        
        return sb.toString();
    }

   
    
    
    
    
    
    /**
     * @return the sureName
     */
    public String getSureName() {
        return sureName;
    }

    /**
     * @param sureName the sureName to set
     */
    public void setSureName(String sureName) {
        this.sureName = sureName;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

   

    /**
     * @return the position
     */
    public String getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * @return the positionPart
     */
    public double getPositionPart() {
        return positionPart;
    }

    /**
     * @param positionPart the positionPart to set
     */
    public void setPositionPart(double positionPart) {
        this.positionPart = positionPart;
    }

    /**
     * @return the salary
     */
    public double getSalary() {
        return salary;
    }

    /**
     * @param salary the salary to set
     */
    public void setSalary(double salary) {
        this.salary = salary;
    }

    /**
     * @return the period
     */
    public LocalDate getPeriod() {
        return period;
    }

    /**
     * @param period the period to set
     */
    public void setPeriod(LocalDate period) {
        this.period = period;
    }

    /**
     * @return the sum
     */
    public double getSum() {
        return sum;
    }

    /**
     * @param sum the sum to set
     */
    public void setSum(double sum) {
        this.sum = sum;
    }
    
    
  
   
    
    
}
