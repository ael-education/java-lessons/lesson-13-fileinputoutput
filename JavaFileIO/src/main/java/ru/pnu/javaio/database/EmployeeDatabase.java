/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.pnu.javaio.database;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Задание перенсти код чтения из файла, записи в файл,  и разбора в класс EmployeeDatabase
 *
 * @author developer
 */
public class EmployeeDatabase {
    
    
    
    /**
     * Карта для хранения списка сотруников
     * 
     *         табельный номер  экземляр класса сотруника
     *             |                  /
     */                      
    private final Map<Integer, Employee> employeeMap = new  ConcurrentHashMap<>();

    
    
    /**
     * Получение доступа к экземпляру employeeMap
     * 
     * @return 
     */
    public Map<Integer, Employee> getEmployeeMap() {
        return employeeMap;
    }

    
    
}
