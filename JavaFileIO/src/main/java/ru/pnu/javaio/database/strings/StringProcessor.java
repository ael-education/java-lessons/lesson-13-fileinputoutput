/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.pnu.javaio.database.strings;

import java.util.StringTokenizer;
import ru.pnu.javaio.database.Employee;

/**
 *
 * @author developer
 */
public class StringProcessor {

    public static Employee parce(String str) {

        StringBuilder sb = new StringBuilder();
        Employee e = null;

        System.out.println("Разбор строки размером  [" + str.getBytes().length + "] байт ");

        StringTokenizer st = new StringTokenizer(str);
        int countTokens = st.countTokens();
        System.out.println("Количество токенов: [" + countTokens + "]");
        
        if (countTokens > 0) {
            e = new Employee();
            System.out.println("Выполнение разбора строки: " + str);
            
            
            int i = -1;
            while (st.hasMoreTokens()) {
                i++;
                String t = st.nextToken();
                System.out.println("токен [" + i + "] = " + t);
                
                if (t != null && !t.isEmpty()) {
                    if (i == 0) {
                        e.setSureName(t);
                        sb.append("Добавлено поле [SureName] = " + e.getSureName());
                        sb.append("\n");
                    }
                    if (i == 1) {
                        e.setFirstName(t);
                        sb.append("Добавлено поле [FirstName] = " + e.getFirstName());
                        sb.append("\n");
                    }
                    if (i == 2) {
                        e.setMiddleName(t);
                        sb.append("Добавлено поле [MiddleName] = " + e.getMiddleName());
                        sb.append("\n");
                    }
                    if (i == 3) {
                        e.setPosition(t);
                        sb.append("Добавлено поле [Position] = " + e.getPosition());
                        sb.append("\n");
                    }
                    if (i == 4) {
                        try {
                            double positionPartValue = Double.parseDouble(t);
                            e.setPositionPart(positionPartValue);
                            sb.append("Добавлено поле [PositionPart] = " + e.getPositionPart());
                            sb.append("\n");
                        } catch (NumberFormatException ex) {
                            System.out.println("Ошибка чтения поля ставки, ошибка преобразования [" + t + "] в число double: " + ex.toString());
                        }

                    }
                    if (i == 5) {
                        try {
                            double salary = Double.parseDouble(t);
                            e.setSalary(salary);
                            sb.append("Добавлено поле [Salary] = " + e.getSalary());
                            sb.append("\n");
                        } catch (NumberFormatException ex) {
                            System.out.println("Ошибка чтения поля оклада по должности, ошибка преобразования [" + t + "] в число double: " + ex.toString());
                        }
                    }
                }               
            }

        } else {
            System.out.println("Строка не имеет токенов - разбору не подлежит");
        }
        
        System.out.println("Лог формирования экземпялра класса сотруник:\n"+sb.toString());
        System.out.println("Прочитан экземпляр класса сотрудник: "+e);
        
        return e;
    }

}
