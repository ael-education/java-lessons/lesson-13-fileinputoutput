/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.pnu.javaio.fileOperation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Задание:   запись в файл форматированной строки
 * 
 * @author developer
 */
public class FileManager {

    private InputStream is;
    private OutputStream os;

    private boolean open;  // Признак успешного открытия файла

    /**
     * Операция открытия файла
     *
     * @param file
     */
    public void openFile(File file) {
        // Проверить существование файла

        System.out.println("Проверка существоваания файла...");
        boolean exists = file.exists();
        if (exists) {
            System.out.println("Файл [" + file.getName() + "] сущесвует");
            System.out.println("Файл существует в каталоге: [" + file.getParent() + "]");
            try {
                is = new FileInputStream(file);
                System.out.println("Потоки к файлу открыты успешно");
                open = true;

            } catch (FileNotFoundException ex) {
                System.out.println("Ошибка: файл не сущесвует");
            }
        }
    }

    /**
     * Операция открытия файла
     *
     * @param file
     */
    public void closeFile(File file) {
        // Проверить существование файла

        System.out.println("Закрытие...");
        boolean exists = file.exists();
        if (exists) {
            System.out.println("Файл [" + file.getName() + "] сущесвует");
            System.out.println("Файл существует в каталоге: [" + file.getParent() + "]");
            try {
                is.close();
                System.out.println("Потоки к файлу закрыты успешно");
            } catch (FileNotFoundException ex) {
                System.out.println("Ошибка: файл не сущесвует");
            } catch (IOException ex) {
                System.out.println("Ошибка ввода-вывода при закрытии файла");
            }
        }
    }
    
    /**
     * Создать новый файл и записать в него строки, предварительно проверив
     * входное имя
     *
     * @param folder имя каталога для размещения файла
     * @param fileName имя файла
     *
     */
    public boolean createFile(String folder, String fileName) {

        // Проверка наличия и присутвия символов в имени файла и имени каталога
        boolean result = true;

        if (folder != null && !folder.isEmpty()) {
            System.out.println("Файл создается в каталоге: " + folder);

        } else {
            result = false;
            System.out.println("Ошибка создания файла: имя каталога пустое");
        }

        //  Cуществует экземпляр?    Логическое И  строка имени файла не пустая
        //     |                         /
        if (fileName != null && !fileName.isEmpty()) {
            System.out.println("Имя файла: " + fileName);

        } else {
            result = false;
            System.out.println("Ошибка создания файла: пустое имя файла");
        }

        /**
         * Разделитель имени каталога в Win \ Разделитель имени каталога в Linux
         * / Для унивесальности используется File.separator
         */
        if (result) {
            File file = new File(folder + File.separator + fileName);
            boolean exists = file.exists();
            if (exists) {
                System.out.println("Файл сущесвует. Отмена создания файла... ");
            } else {
                try {
                    boolean createResult = file.createNewFile();
                    if (createResult) {
                        System.out.println("Файл [" + file.getName() + "] создан успешно, каталог размещения: " + file.getParent());
                    } else {
                        result = false;
                        System.out.println("Ошибка: файл не создан");
                    }

                } catch (IOException ex) {
                    result = false;
                    System.out.println("Ошибка создания файла: " + ex.toString());
                }
            }
        }
        return result;
    }
    

    /**
     * Чтение массива строк из файла
     *
     * @param file
     * @return
     */
    public List<String> readStrings(File file) {

        List<String> records = new ArrayList<>();
        if (open) {
            // Определение списка для хранения записей из файла
            System.out.println("Чтение строк из файла [" + file.getName() + "]..");
            Scanner scan = new Scanner(is);
            System.out.println("Открыт ");
            
            while (scan.hasNextLine()) {
                String str = scan.nextLine();
                if (str != null && !str.isEmpty()) {
                    records.add(str);
                }
                System.out.println("Прочитана строка ==> " + str);
            }

            System.out.println("Чтение завершено, прочитано [" + records.size() + "] строк");
            scan.close();
        } else {
            System.out.println("Чтение строки из файла невозможно - не получен доступ к файлу");
        }
        return records;
    }

}
